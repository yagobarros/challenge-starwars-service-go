package main

import (
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/yagobarros/challenge-starwars-service-go/rest/planets"
	"github.com/gin-gonic/gin"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	serverAPI()
}

func serverAPI() {
	server := gin.Default()

	// rotas
	server.GET("/api/v1/planets", timeRequest, planets.ListPlanets)
	server.GET("/api/v1/planets/view", timeRequest, planets.ViewPlanet)
	server.POST("/api/v1/planets/create", timeRequest, planets.CreatePlanet)
	server.DELETE("/api/v1/planets/:uuid", timeRequest, planets.DeletePlanet)

	// sobe serviço na porta definida na variavel de ambiente =>  GO_PORT  <= dentro de local.sh
	server.Run(":" + os.Getenv("GO_PORT"))
}

// timeRequest retorna inclui no log o tempo de execução da requisição.
func timeRequest(ctx *gin.Context) {
	startTime := time.Now()
	ctx.Next()
	duration := time.Now().Sub(startTime)
	log.Printf("tempo de execução da requisiçao: %v \n", duration)
}

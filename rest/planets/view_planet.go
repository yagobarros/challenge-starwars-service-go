package planets

import (
	"log"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/yagobarros/challenge-starwars-service-go/lib"
	"bitbucket.org/yagobarros/challenge-starwars-service-go/utils"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/gin-gonic/gin"
)

// ViewPlanet exported => busca planeta por ID ou Nome
func ViewPlanet(ctx *gin.Context) {
	tableName := os.Getenv("TABLE_NAME")
	log.Println("buscando planeta por uuid ou nome")
	service := utils.ConnectToDynamo()
	var planet lib.Planet
	reqUUID := ctx.Query("uuid")
	reqName := ctx.Query("name")

	if utils.IsEmpty(reqUUID) && utils.IsEmpty(reqName) {
		log.Println("uuid ou nome não informado")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"code":    "x011",
			"message": "A Requisição deve conter identificador ou nome do planeta",
		})
		return
	}

	if !utils.IsEmpty(reqUUID) && !utils.IsEmpty(reqName) {
		log.Println("uuid e nome informado. deve ser apenas um.")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"code":    "x012",
			"message": "A Requisição deve conter apenas identificador ou apenas nome do planeta",
		})
		return
	}

	if !utils.IsEmpty(reqUUID) && utils.IsEmpty(reqName) {
		log.Printf("iniciando busca por uuid => %v \n", reqUUID)
		result, err := service.GetItem(&dynamodb.GetItemInput{
			TableName: aws.String(tableName),
			Key: map[string]*dynamodb.AttributeValue{
				"uuid": {
					S: aws.String(reqUUID),
				},
			},
		})
		if err != nil {
			log.Println(err.Error())
			ctx.JSON(http.StatusBadRequest, gin.H{
				"status":  http.StatusBadRequest,
				"code":    "x013",
				"message": err.Error(),
			})
			return
		}

		if len(result.Item) == 0 {
			log.Println("nenhum resultado encontrado")
			ctx.JSON(http.StatusBadRequest, gin.H{
				"status":  http.StatusBadRequest,
				"code":    "x014",
				"message": "Nenhum planeta encontrado",
			})
			return
		}
		err = dynamodbattribute.UnmarshalMap(result.Item, &planet)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"status":  http.StatusBadRequest,
				"code":    "x015",
				"message": "Erro ao converter payload",
			})
			return
		}
	}

	if !utils.IsEmpty(reqName) && utils.IsEmpty(reqUUID) {
		log.Println("iniciando busca por nome")
		var queryInput = &dynamodb.QueryInput{
			TableName: aws.String(tableName),
			IndexName: aws.String("name-index"),
			KeyConditions: map[string]*dynamodb.Condition{
				"name": {
					ComparisonOperator: aws.String("EQ"),
					AttributeValueList: []*dynamodb.AttributeValue{
						{
							S: aws.String(strings.ToLower(reqName)),
						},
					},
				},
			},
		}
		result, err := service.Query(queryInput)
		if len(result.Items) > 0 {
			err = dynamodbattribute.UnmarshalMap(result.Items[0], &planet)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{
					"status":  http.StatusBadRequest,
					"code":    "x016",
					"message": "Erro ao converter payload",
				})
				return
			}
		}

		if len(result.Items) == 0 {
			log.Println("nenhum resultado encontrado")
			ctx.JSON(http.StatusBadRequest, gin.H{
				"status":  http.StatusBadRequest,
				"code":    "x017",
				"message": "Nenhum planeta encontrado",
			})
			return
		}
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data":   planet,
		"status": http.StatusOK,
	})
	return
}

package planets

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/yagobarros/challenge-starwars-service-go/utils"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/gin-gonic/gin"
)

// DeletePlanet exported => deleta um planeta por uuid especificado
func DeletePlanet(ctx *gin.Context) {
	tableName := os.Getenv("TABLE_NAME")
	log.Println("Deletando um planeta por uuid")
	service := utils.ConnectToDynamo()
	uuid := ctx.Param("uuid")

	if utils.IsEmpty(uuid) {
		log.Println("uuid nao informado")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"code":    "x018",
			"message": "UUID não informado",
		})
		return
	}

	log.Printf("iniciando busca por uuid => %v \n", uuid)
	result, err := service.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"uuid": {
				S: aws.String(uuid),
			},
		},
	})
	if err != nil {
		log.Println(err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"code":    "x019",
			"message": err.Error(),
		})
		return
	}

	if len(result.Item) == 0 {
		log.Println("nenhum resultado encontrado")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"code":    "x020",
			"message": fmt.Sprintf("Nenhum planeta encontrado com identificador %v", uuid),
		})
		return
	}

	if len(result.Item) > 0 {
		log.Printf("Deletando planeta com uuid: %v\n", uuid)
		_, err = service.DeleteItem(&dynamodb.DeleteItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"uuid": {
					S: aws.String(uuid),
				},
			},
			TableName: aws.String(tableName),
		})

		if err != nil {
			log.Println(err.Error())
			ctx.JSON(http.StatusBadRequest, gin.H{
				"status":  http.StatusBadRequest,
				"code":    "x021",
				"message": "Ocorreu um erro inesperado ao tentar excluir planeta",
			})
			return
		}

		log.Println("excluido com sucesso")
		ctx.JSON(http.StatusOK, gin.H{
			"status": http.StatusOK,
			"data":   "Planeta deletado com sucesso",
		})
		return
	}

}

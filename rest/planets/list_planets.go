package planets

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/yagobarros/challenge-starwars-service-go/lib"
	"bitbucket.org/yagobarros/challenge-starwars-service-go/utils"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/gin-gonic/gin"
)

// ListPlanets exported => Busca planetas cadastrados na base e retorna [ 'NOME', 'CLIMA', 'TERRENO', 'QUANTIDADE DE APIRAÇÕES EM FILME']
func ListPlanets(ctx *gin.Context) {
	tableName := os.Getenv("TABLE_NAME")
	log.Println("Buscando planetas cadastrados na base")

	database := utils.ConnectToDynamo()
	params := &dynamodb.ScanInput{
		TableName: aws.String(tableName),
	}

	log.Println("executando a busca")
	result, err := database.Scan(params)
	if err != nil {
		log.Println("Query API call failed:")
		log.Printf("\n x001 - %v", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"code":    "x001",
			"message": err.Error(),
		})
		return
	}

	var items []interface{}
	log.Println("percorrendo todos os planetas e colocando na estrutura Planets")
	for _, i := range result.Items {
		var item lib.Planet
		err = dynamodbattribute.UnmarshalMap(i, &item)
		if err != nil {
			log.Println("Got error unmarshalling:")
			log.Printf("x002 - %v", err.Error())
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"status":  http.StatusInternalServerError,
				"code":    "x002",
				"message": err.Error(),
			})
			return
		}

		if !utils.IsEmpty(item.URL) {
			err := item.CountFilms()
			if err != nil {
				log.Printf("x005 - %v", err.Error())
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"status":  http.StatusInternalServerError,
					"code":    "x005",
					"message": err.Error(),
				})
				return
			}
		}

		items = append(items, item)
	}

	log.Println("Retornando Planetas encontrados")
	log.Printf("%v", items)
	ctx.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data":   items,
	})
	return
}

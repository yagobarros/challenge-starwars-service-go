package planets

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/yagobarros/challenge-starwars-service-go/lib"
	"bitbucket.org/yagobarros/challenge-starwars-service-go/utils"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

// CreatePlanet exported - cria um planeta na base, usando como parametros ['name', 'climate', 'terrain']
func CreatePlanet(ctx *gin.Context) {
	tableName := os.Getenv("TABLE_NAME")
	log.Println("Criando um planeta")
	service := utils.ConnectToDynamo()
	var planet lib.Planet
	log.Println("recebendo payload")
	ctx.BindJSON(&planet)

	log.Println("criando uuid")
	planet.UUID = uuid.NewV4().String()
	log.Printf("UUID criado: %v \n", planet.UUID)

	if utils.IsEmpty(planet.Name) {
		log.Println("x006 - nome do planeta invalido")
		ctx.JSON(400, gin.H{
			"status":  http.StatusBadRequest,
			"code":    "x006",
			"message": "Erro ao cadastrar. Nome inválido",
		})
		return
	}

	if len(planet.Terrain) == 0 {
		log.Println("x007 - terreno invalido.")
		ctx.JSON(400, gin.H{
			"status":  http.StatusBadRequest,
			"code":    "x007",
			"message": "Erro ao cadastrar. Clima inválido",
		})
		return
	}

	if len(planet.Climate) == 0 {
		log.Println("x008 - clima invalido")
		ctx.JSON(400, gin.H{
			"status":  http.StatusBadRequest,
			"code":    "x008",
			"message": "Erro ao cadastrar. Clima inválido",
		})
		return
	}

	planet.Name = strings.ToLower(planet.Name)

	for i := 0; i < len(planet.Terrain); i++ {
		planet.Terrain[i] = strings.ToLower(planet.Terrain[i])
		fmt.Println(planet.Terrain[i])
	}
	for i := 0; i < len(planet.Climate); i++ {
		planet.Climate[i] = strings.ToLower(planet.Climate[i])
	}

	log.Println("convertendo payload para estrutura dynamo")
	payload, err := dynamodbattribute.MarshalMap(planet)
	if err != nil {
		log.Println("erro ao converter planeta:")
		log.Printf("x009 - %v", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"code":    "x009",
			"message": err.Error(),
		})
		return
	}

	log.Println("definindo parametros de criação")
	input := &dynamodb.PutItemInput{
		Item:      payload,
		TableName: aws.String(tableName),
	}

	log.Println("criando um planeta na base")
	_, err = service.PutItem(input)
	if err != nil {
		log.Println("Got error calling PutItem:")
		log.Printf("x010 - %v", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"code":    "x010",
			"message": err.Error(),
		})
		return
	}

	log.Println("planeta criado com sucesso")
	ctx.JSON(http.StatusCreated, gin.H{
		"status": http.StatusCreated,
		"data":   "Planeta cadastrado com sucesso",
	})
	return
}

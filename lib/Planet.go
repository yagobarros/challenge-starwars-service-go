package lib

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

// Planet exported => Estrutura de Exibição de planetas
type Planet struct {
	UUID     string   `json:"uuid"`
	Name     string   `json:"name"`
	Climate  []string `json:"climate"`
	Terrain  []string `json:"terrain"`
	QtyFilms int      `json:"qty_films,omitempty"`
	URL      string   `json:"url,omitempty"`
}

// CountFilms exported => busca na API swapi pois se tiver algum lancamento de filme estará sempre atualizado.
func (p *Planet) CountFilms() error {
	log.Printf("Buscando aparições do planeta %v em filmes", p.Name)
	var item map[string][]interface{}
	var client http.Client
	resp, err := client.Get(p.URL)
	if err != nil {
		log.Printf("x003 - %v", err.Error())
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Printf("x004 - %v", err.Error())
			return err
		}
		json.Unmarshal(bodyBytes, &item)
		p.QtyFilms = len(item["films"])
	}
	return nil
}

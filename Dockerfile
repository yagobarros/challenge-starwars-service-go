FROM alpine:3.7

RUN apk update && apk add --no-cache unzip ca-certificates

# copiando pasta terraform com arquivos para executar os comando de terraform caso nao haja na maquina.  
COPY ./terraform ./

# install Terraform
ADD https://releases.hashicorp.com/terraform/0.12.19/terraform_0.12.19_linux_amd64.zip ./
RUN unzip terraform_0.12.19_linux_amd64.zip
RUN rm -rf terraform_0.12.19_linux_amd64.zip
RUN install terraform /usr/local/bin

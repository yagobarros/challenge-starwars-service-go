# Provider
 provider "aws" {
   access_key = "AWS_ACCESS_KEY_ID"
   secret_key = "AWS_SECRET_ACCESS_KEY"
   region = "us-west-2" // REGIÃO AWS
 }


#Insert item dynamodb
resource "aws_dynamodb_table_item" "service" {

  table_name = "${aws_dynamodb_table.service.name}"
  hash_key   = "${aws_dynamodb_table.service.hash_key}"
  
  item = <<ITEM
{
  "climate": {
    "L": [
      {
        "S": "arid"
      }
    ]
  },
  "name": {
    "S": "terra"
  },
  "terrain": {
    "L": [
      {
        "S": "jungle"
      }
    ]
  },
  "uuid": {
    "S": "e84350d5-0f09-4a87-a639-39a8ec0eb064"
    }
}
ITEM
}

 #DynamoDB
 resource "aws_dynamodb_table" "service" {
  name           = "planets"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "uuid"

  attribute {
    name = "uuid"
    type = "S"
  }

  attribute {
    name = "name"
    type = "S"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  global_secondary_index {
    name               = "name-index"
    hash_key           = "name"
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "ALL"
  }
}

# API STAR WARS
    TECNOLOGIAS UTILIZADAS: 
    - GoLang
    - Go Gin HTTP framework 
    - DynamoDB 
    - Docker
    - Terraform

    Dentro do arquivo local.sh estão as variaveis de ambiente como Região e Credenciais da AWS para utilização do dynamo. Porta que a api está rodando.
    
    - Torne os arquivos build.sh e local.sh executaveis.

    Para facilitar criação do banco foi utilizado o terraform.
    Certifique-se que não há nenhuma tabela criada com o nome "planets", caso tenha altere o arquivo terraform/main.tf e a variável TABLE_NAME dentro de local.sh
    para usá-lo configure o arquivo terraform/main.tf com suas credenciais, logo após execute os seguintes comandos: 
        - ./build.sh  "este comando irá executar o docker build"
        - docker run -it yagobarros/terraform "este comando irá acessar a imagem"
       Dentro da imagem execute: 
        - terraform init "este comando irá iniciar o terraform"
        - terraform plan
        - terraform apply "siga o passo a passo mostrado no terminal e terá seu banco configuração para execução da aplicação."
    
    Dentro do banco dynamodb foi criado uma tabela chamada: "planets".
    criando um indice chamado: "name-index" e atributindo como chave "name".
    mantendo os itens com a seguinte estrutura: 

    {
        "uuid": "e84350d5-0f09-4a87-a639-39a8ec0eb064"
        "name": "tatooine",
        "terrain": ["dessert"],
        "climate": ["arid"],
        "url": "https://swapi.co/api/planets/1/"
    }

    a url se mantem para efetuar a busca do número de aparições em filmes.

    - Adicionar um planeta (com nome, clima e terreno) 
        Rota: /api/v1/planets/create   [METHOD POST]
        Payload: 
            {
                name: STRING (OBRIGATÓRIO),
                climate: ARRAY[STRING] (OBRIGATÓRIO),
                terrain: ARRAY[STRING] (OBRIGATÓRIO)
            }
        Exemplo de payload: 
            {
                "name": "Earth",
                "climate": ["Arid"],
                "terrain": ["jungle"]
            }

    - Listar planetas
        Rota: /api/v1/planets/   [METHOD GET]
            
    - Buscar por nome
        Rota: /api/v1/planets?name=NOME_DO_PLANETA  [METHOD GET]

    - Buscar por UUID
        Rota: /api/v1/planets?uuid=UUID_DO_PLANETA  [METHOD GET]
        
    - Remover planeta
        Rota: /api/v1/planets/:uuid  [METHOD DELETE]



    Para executar o projeto execute no terminal o comando  ./local.sh




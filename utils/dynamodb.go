package utils

import (
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// ConnectToDynamo Exported
func ConnectToDynamo() *dynamodb.DynamoDB {
	log.Println("conectando na base de dados.")
	if _, ok := os.LookupEnv("AWS_REGION"); !ok {
		panic("AWS_REGION not found")
	}

	config := &aws.Config{
		Region: aws.String(os.Getenv("AWS_REGION")),
	}

	newSession := session.Must(session.NewSession(config))
	service := dynamodb.New(newSession)
	log.Println("base de dados = ok!")
	return service
}
